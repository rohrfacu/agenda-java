package com;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.json.JSONException;
import org.json.JSONObject;

@Entity
@XmlRootElement
public class Contacto {
	@XmlElement(name = "nombre")
	String nombre;
	@Id
	@XmlElement(name = "nro")
	long nro;
	
	public Contacto() {
		
	}
	public Contacto(String nombre, long nro) {
		this.nombre = nombre;
		this.nro = nro;
	}
	@Override
	public String toString() {
		try {
			return new JSONObject().put("nombre", nombre).put("nro", nro).toString();
		} catch (JSONException ex) {
			return null;
		}
	}
	
	
}
