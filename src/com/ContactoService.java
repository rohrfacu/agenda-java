package com;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.json.JSONException;

@Path("/contactos")
public class ContactoService {
	private static final long serialVersionUID = 1L;
	
	EntityManagerFactory emf;
	EntityManager em;
	
	public ContactoService() {
		this.emf = Persistence.createEntityManagerFactory("agendaPU");
		this.em = emf.createEntityManager();
		
		Contacto c1 = new Contacto("pedro", 303030);
		
		EntityTransaction tx = em.getTransaction();
		
		try {
			tx.begin();
			
			em.persist(c1);
			
			tx.commit();
		} catch (Exception e) {
			if(tx.isActive())
				tx.rollback();
		}
		
		em.persist(c1);
	}
	
	@GET
	@Produces("application/json")
	public Response getContactos() throws JSONException{
		List<Contacto> contactos = new ArrayList<Contacto>();
		TypedQuery<Contacto> q = em.createQuery("SELECT u FROM Contacto u", Contacto.class);
		contactos = q.getResultList();
		em.close();
		emf.close();
		
		return Response.status(200).entity(contactos.toString())
				.header("Access-Control-Allow-Origin", "*")
				.build();
	}
}
